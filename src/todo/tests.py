from django.test import TestCase

from .models import Todo


class TodoTest(TestCase):
    def setUp(self):
        todo1 = Todo.objects.create(todo_text='Hello')
        todo1.save()

    def test_todo_new(self):
        new_todo = Todo.objects.create(todo_text='Hi')
        new_todo.save()
        self.assertEqual(Todo.objects.all().count(), 2)

    def test_todo_delete(self):
        todo = Todo.objects.get(id=1)
        todo.delete()
        self.assertEqual(Todo.objects.all().count(), 0)

    def test_todo_update(self):
        todo = Todo.objects.get(id=1)
        todo.todo_text = 'Howdy'
        todo.save()
        self.assertEqual(Todo.objects.all().count(), 1)
        self.assertEqual(Todo.objects.get(id=1).todo_text, 'Howdy')
